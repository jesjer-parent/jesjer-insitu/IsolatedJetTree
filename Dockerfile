# analysis base version from which we start
FROM atlas/analysisbase:21.2.113

# put the code into the repo with the proper owner
COPY --chown=atlas . /code/src

# note that the build directory already exists in /code/src
RUN sudo chown -R atlas /code/src/build && \
    cd /code/src/build && \
    source /home/atlas/release_setup.sh && \
    cmake /code/src/source && \
    make -j4

# where you are left after booting the image
WORKDIR /code/src

# Lines needed to run with reana
#USER root
#RUN sudo usermod -G root atlas
#USER atlas
