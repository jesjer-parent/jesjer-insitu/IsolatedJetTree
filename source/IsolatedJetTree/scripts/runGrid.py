#!/usr/bin/python
import os, sys
from time import strftime

test = False

timestamp = strftime("_%d%m%y")
if not test:
  if not os.path.exists("gridOutput"):
    os.system("mkdir gridOutput")
  if not os.path.exists("gridOutput/gridJobs"):
    os.system("mkdir gridOutput/gridJobs")

config_name = "source/IsolatedJetTree/data/config_Tree_withCS.py"
extraTag = "" # Extra output tag for all files

## Set this only for group production submissions ##
production_name = ""
#production_name = "perf-jets"
#production_name = "phys-exotics"

samples = {
#  "Data15"     : "data15_13TeV.period*.physics_Main.PhysCont.DAOD_JETM1.grp15_v01_p3319",
#  "Data16"     : "data16_13TeV.period*.physics_Main.PhysCont.DAOD_JETM1.grp16_v01_p3319",
#  "Pythia"     : "mc16_13TeV.3610*.Pythia8*JZ*W.*JETM1.*_r9364_r9315_p3260",
#  "Herwig"     : "mc16_13TeV.3644*.Herwig7EvtGen_H7UE_NNPDF30nlo_jetjet_JZ*.deriv.DAOD_JETM1.*_r9364_r9315_p3260",
#  "MC16c"      : "mc16_13TeV.3610*.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*W.deriv.DAOD_JETM1.*_r9781_r9778_p3260",
  "MC16d"      : "mc16_13TeV.3610*.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*W.deriv.DAOD_JETM1.*_r10201_r10210_p3401",
#  "AFII"      : "mc16_13TeV.3610*.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*W.*JETM1.*_a875_r9364_r9315_p3371",
#  "Pythia"     : "mc16_13TeV.3610*.Pythia8*JZ*W.*JETM6*_r9315_p3260",
#  "LocalJZ4"      : "../../../Dijet/inputFiles/mc16_JZ4_Rel21/",
}

#### Driver options ####
runType = 'grid'   #CERN grid
#runType = 'local'
#runType = 'condor' #Uchicago Condor

files = []
outputTags = []

##################################################################################
#
##################################################################################

for sampleName, sample in samples.iteritems():

  output_tag = sampleName + extraTag + timestamp
  submit_dir = "gridOutput/gridJobs/submitDir_"+output_tag

  ## Configure submission driver ##
  driverCommand = ''
  if runType == 'grid':
    driverCommand = 'prun --optSubmitFlags="--forceStaged" --optGridOutputSampleName='
    #driverCommand = 'prun --optGridMaxNFilesPerJob=2 --optSubmitFlags="--forceStaged" --optGridOutputSampleName='
    #driverCommand = 'prun --optSubmitFlags="--skipScout --excludedSite=ANALY_CERN_SHORT,ANALY_BNL_SHORT" --optGridOutputSampleName='
    if len(production_name) > 0:
      #driverCommand = ' prun --optSubmitFlags="--memory=5120 --official --skipScout" --optGridOutputSampleName='
      driverCommand = ' prun --optSubmitFlags="--official --forceStaged" --optGridOutputSampleName='
      #driverCommand = ' prun --optGridMaxNFilesPerJob=2 --optSubmitFlags="--official --forceStaged" --optGridOutputSampleName='
      #driverCommand = ' prun --optSubmitFlags="--official" --optGridOutputSampleName='
      driverCommand += 'group.'+production_name
    else:
      driverCommand += 'user.%nickname%'
    driverCommand += '.%in:name[2]%.'+output_tag
    #!driverCommand += '.%in:name[1]%.%in:name[2]%.'+output_tag
    #driverCommand += '.%in:name[1]%.%in:name[2]%.%in:name[3]%.'+output_tag
  elif runType == 'condor':
    driverCommand = ' condor --optFilesPerWorker 10 --optBatchWait'
  elif runType == 'local':
    driverCommand = ' direct'


  command = './source/xAODAnaHelpers/scripts/xAH_run.py'
  if runType == 'grid':
    command += ' --inputRucio '

  if 'sampleLists' in sample:
    command += ' --inputList'
  command += ' --files '+sample
  command += ' --config '+config_name
  command += ' --force --submitDir '+submit_dir
  command += ' '+driverCommand


  print command
  if not test: os.system(command)
