#ifndef IsolatedJetTree_TreeTemplate_H
#define IsolatedJetTree_TreeTemplate_H
/** @file TreeTemplate.h
 *  @brief Define and fill TTree branches for Isolated Jets
 *  @author Jeff Dandoy
 */

#include "xAODAnaHelpers/HelpTreeBase.h"
#include "TTree.h"

/**
  @brief Define and fill the TTrees.  Inherits from xAODAnaHelpers::HelpTreeBase
*/
class TreeTemplate : public HelpTreeBase
{

  private:

    /** @brief Additional weight info (set w/ detailStr in config) */
    bool m_add_weight  = false;
    /** @brief Store number of jets with more than 20GeV */
    bool m_add_njet20  = false;
    /** @brief Total number of jets with more than 20GeV */
    int m_njet20;
    /** @brief Total weight of the event, a multiplication of other event weights*/
    float m_weight;
    /** @brief Cross-section weight of the event */
    float m_weight_xs;
    /** @brief MC simulation weight of the event */
    float m_weight_mcEventWeight;
    /** @brief Pileup weight of the event in MC */
    float m_weight_pileup;
    /** @brief Event pt density */
    float m_rho;

    /** @brief mass or each jet */
    std::vector<float> m_jet_m;
    /** @brief Detector \f$\eta\f$ of each jet */
    std::vector<float> m_jet_DetEta;
    /** @brief Jvt for each jet */
    std::vector<float> m_jet_Jvt;

    // Variable for MC samples
    /** @brief PartonTruthLabelID for each jet */
    std::vector<int> m_jet_PartonTruthLabelID;
    /** @brief Kinematics of matched truth jet */
    std::vector<float> m_jet_true_pt;
    std::vector<float> m_jet_true_eta;
    std::vector<float> m_jet_true_phi;
    std::vector<float> m_jet_true_e;
    std::vector<float> m_jet_true_m;
    /** @brief Reco vs Truth ratios */
    std::vector<float> m_jet_respE;
    std::vector<float> m_jet_respPt;

    bool m_add_jetmass = false;

    /** @brief Variables used by the GSC */
    bool m_add_GSCVars = false;
    std::vector<int> m_jet_Ntrk1000;
    std::vector<float> m_jet_Wtrk1000;
    std::vector< std::vector<float> > m_jet_EnergyPerSampling;
    std::vector<float> m_jet_ChargedFraction;
    std::vector<int> m_jet_nMuSeg;
    std::vector<float> m_jet_n90Constituents;

    /** @brief Jet kinematics before calibration (detailStr m_add_JetCalibStages) */
    bool m_add_JetCalibStages = false;
    /** @brief Jet kinematics at ConstitScale (detailStr m_add_JetConstitScale) */
    bool m_add_JetConstitScale = false;
    /** @brief Jet kinematics at PileupScale (detailStr m_add_JetPileupScale) */
    bool m_add_JetPileupScale = false;
    /** @brief Jet kinematics at JESScale (detailStr m_add_JetJESScale) */
    bool m_add_JetJESScale = false;
    std::vector<float> m_jet_ConstitPt;
    std::vector<float> m_jet_ConstitEta;
    std::vector<float> m_jet_ConstitPhi;
    std::vector<float> m_jet_ConstitE;
    std::vector<float> m_jet_ConstitMass;
    std::vector<float> m_jet_PileupPt;
    std::vector<float> m_jet_PileupEta;
    std::vector<float> m_jet_PileupPhi;
    std::vector<float> m_jet_PileupE;
    std::vector<float> m_jet_PileupMass;
    std::vector<float> m_jet_EMPt;
    std::vector<float> m_jet_EMEta;
    std::vector<float> m_jet_EMPhi;
    std::vector<float> m_jet_EME;
    std::vector<float> m_jet_EMMass;
    std::vector<float> m_jet_JESPt;
    std::vector<float> m_jet_JESEta;
    std::vector<float> m_jet_JESPhi;
    std::vector<float> m_jet_JESE;
    std::vector<float> m_jet_JESMass;

    /** @brief Track variables for various track pt thresholds */
    bool m_add_MoreTrackMoments = false;
    std::vector<int> m_jet_Ntrk500;
    std::vector<float> m_jet_Wtrk500;
    std::vector<int>   m_jet_Ntrk2000;
    std::vector<int>   m_jet_Ntrk3000;
    std::vector<int>   m_jet_Ntrk4000;
    std::vector<float> m_jet_Wtrk2000;
    std::vector<float> m_jet_Wtrk3000;
    std::vector<float> m_jet_Wtrk4000;


    /** @brief Isolation variables */
    bool m_add_isolation = false;
    std::vector<int> m_jet_iso_dR;
    std::vector<float> m_jet_ghostFrac;



  public:

    /** @brief Create the base HelpTreeBase instance */
    TreeTemplate(xAOD::TEvent * event, TTree* tree, TFile* file);
    /** @brief Standard destructor*/
    ~TreeTemplate();

    /** @brief Connect the branches for event-level variables */
    void AddEventUser( const std::string detailStr = "" );
    /** @brief Connect the branches for jet-level variables */
    void AddJetsUser( const std::string detailStr = "" , const std::string jetName = "jet");
    /** @brief Fill the TTree with event-level variables */
    void FillEventUser( const xAOD::EventInfo* eventInfo );
    /** @brief Fill the TTree with jet-level variables */
    void FillJetsUser( const xAOD::Jet* jet, const std::string jetName = "jet" );
    /** @brief Clear vectors used by event-level variables*/
    void ClearEventUser();
    /** @brief Clear vectors used by jet-level variables*/
    void ClearJetsUser(const std::string jetName = "jet");

};
#endif
