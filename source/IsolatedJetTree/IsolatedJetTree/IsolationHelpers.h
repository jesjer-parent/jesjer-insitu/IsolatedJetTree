// this file is -*- C++ -*-
#ifndef IsolatedJetTree_ISOLATIONHELPERS_H
#define IsolatedJetTree_ISOLATIONHELPERS_H

#include "Math/GenVector/VectorUtil.h"

namespace IsolatedJetTree {

  //////////////////////////////////////////////////////////////////////
  /// \class IsoHelper
  ///
  /// This class implements the isolation and truth matching procedure.
  /// The procedure are parametrized by a few options given in the ctor (truth, reco , isolation DeltaR,...)
  /// The main functions returns a IsoHelper::Result objects summarizing all the information.
  ///
  /// These functions are templated so that they can be used identically with a JetContainer or a vector<const xAOD::Jet*>
  ///
  struct IsoHelper {

    struct Result {
      bool isTruthMatched = false;
      const xAOD::Jet* matchedTruthJet = nullptr;
      bool isTruthIsolated = false;
      bool isRecoIsolated = false;
      int status = 0 ; // simplest way of reporting errors duting matching or isolation checks;
    };

      

    IsoHelper(const std::string& scale, float truthDr, float truthIsoDr, float recoIsoDr, bool useTruthGhost=false, float ghostFracMax=0.75):
      m_scale(scale)
      , m_truthDr2(truthDr*truthDr)
      , m_truthIsoDr2(truthIsoDr*truthIsoDr)
      , m_recoIsoDr2(recoIsoDr*recoIsoDr)
      , m_useTruthGhost(useTruthGhost)
      , m_ghostFracMax(ghostFracMax)
    {
      if( m_scale== "") m_scale = "JetAssignedScaleMomentum"; // calling xAOD::Jet::jetP4("JetAssignedScaleMomentum" ) is equivalent to jet4()
    }

    
    /// return true if 'jet' if no jet 'jetCont' are closer than dr2. 
    template<typename CONT>
    bool isIsolated(const xAOD::Jet* jet, CONT & jetCont, float dr2, const xAOD::Jet * ignoreJet=nullptr) {
      bool iso_dR = true;
      xAOD::JetFourMom_t refScale = jet->jetP4(m_scale);
      for( const xAOD::Jet* thisJet : jetCont ){
	
	// skip self 
	if (thisJet == jet )
          continue;
	// skip ignored
	if (thisJet == ignoreJet )
          continue;
	
	xAOD::JetFourMom_t thisJetScale = thisJet->jetP4(m_scale) ;

	if( ROOT::Math::VectorUtil::DeltaR2(refScale,thisJetScale) < dr2) {
	  iso_dR=false;
	  break;
	}
      }
      return iso_dR;
    }


    /// returns the closest jet to 'jet' from 'truthCont' which is also deltaR < 'dr2Cut'. Returns nullptr if no match found.
    template<typename CONT>
    const xAOD::Jet* truthMatch(const xAOD::Jet* jet, CONT & truthCont, float dr2Cut) {
      xAOD::JetFourMom_t ref = jet->jetP4(m_scale);
      const xAOD::Jet* matchedTruth = nullptr;
      double minDr2 = dr2Cut; // make sure we find a match below the requirement.
      for( const xAOD::Jet* truJet : truthCont ){

	xAOD::JetFourMom_t truP4 = truJet->jetP4();
	double dr2 = ROOT::Math::VectorUtil::DeltaR2(truP4, ref);
	if( dr2 < minDr2 ){
	  minDr2 = dr2;
	  matchedTruth = truJet;
	}

      }
      return matchedTruth;
    }


    /// calculate isolations and truth matching for 'jet'.
    template<typename CONT>
    Result checkTruthMatchAndIso(const xAOD::Jet* jet, CONT & truthCont, CONT & recoCont){
      Result r;

      if(m_useTruthGhost){ // use ghost associated truth
	// If no associated truth jet, skip this event
	if( !jet->isAvailable< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink") ||
	    !jet->auxdata< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink").isValid() ){
	  r.status  =1;
	} else {
	    float ghostFraction = jet->auxdata<float>("GhostTruthAssociationFraction");
	    r.matchedTruthJet = *jet->auxdata< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink");
	    r.isTruthMatched =  (ghostFraction < m_ghostFracMax);         	   
	}
      }else{  // use deltaR matching 
	const xAOD::Jet* matchedTruthJet = truthMatch(jet, truthCont, m_truthDr2 );
	r.isTruthMatched = (matchedTruthJet != nullptr);
	r.matchedTruthJet = matchedTruthJet;
      }
      
      if(r.isTruthMatched) r.isTruthIsolated = isIsolated(jet, truthCont, m_truthIsoDr2, r.matchedTruthJet);
      if(r.isTruthIsolated) r.isRecoIsolated = isIsolated(jet, recoCont, m_recoIsoDr2);	
            
      return r;
    }

    /// same as above but only check isolation for a reco container (support running on data)
    template<typename CONT>
    Result checkTruthMatchAndIso(const xAOD::Jet* jet,  CONT & recoCont){
      Result r;
      r.isRecoIsolated = isIsolated(jet, recoCont, m_recoIsoDr2);
      return r;
    }


    
    std::string m_scale;
    float m_truthDr2;
    float m_truthIsoDr2;
    float m_recoIsoDr2;
    bool m_useTruthGhost;
    float m_ghostFracMax;
  };
  

}


#endif
