#ifndef IsolatedJetTree_IsolatedJetAlgo_H
#define IsolatedJetTree_IsolatedJetAlgo_H

/** @file IsolatedJetAlgo.h
 *  @brief Run the Isolated Jet  event selection
 *  @author Jeff Dandoy
 */

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

// ROOT include(s):
#include "TH1D.h"

// inlude the parent class header for tree
#ifndef __MAKECINT__
#include "IsolatedJetTree/TreeTemplate.h"
#endif

#include <sstream>


#include "AsgTools/AnaToolHandle.h"

#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"

namespace InDet {
  class IInDetTrackSelectionTool;
}

// variables that don't get filled at submission time should be
// protected from being send from the submission node to the worker
// node (done by the //!)
class IsolatedJetAlgo : public xAH::Algorithm
//class IsolatedJetAlgo : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:

    float m_jetRadius;

    /** @brief Require halfPt isolation to be applied to the TTree */
    bool m_requireIso = true;
    /** @brief Require dPVz cut */
    bool m_applyVertexCut = true;

    /** @brief TEvent object */
    xAOD::TEvent *m_event;  //!
    /** @brief TStore object for variable storage*/
    xAOD::TStore *m_store;  //!
    /** @brief Count of the current event*/
    int m_eventCounter;     //!
    /** @brief Global name to give to the algorithm*/
    std::string m_name;

////// configuration variables set by user //////
    /** @brief Input container name for jet collection*/
    std::string m_inContainerName_jets;
    /** @brief Name of the truth xAOD container for MC Pileup Check, set to "None" if not used */
    std::string m_inContainerName_truth;

    /** @brief Choose primary vertex container for NPV calculation */
    std::string m_vertexContainerName = "PrimaryVertices";
    /** @brief Control substrings for creating event-level histograms */
    std::string m_eventDetailStr;
    /** @brief Control substrings for creating jet-level histograms */
    std::string m_jetDetailStr;
    /** @brief Control substrings for creating trigger-level histograms */
    std::string m_trigDetailStr;
    /** @brief Set verbose mode */
    bool m_debug;
    bool m_addTrackMoments = false;
    /** @brief Setting for ATLAS Fastsim production samples */
    bool m_isAFII;
    /** @brief Option to output the cutflow histograms */
    bool m_useCutFlow;

    /** @brief Use dR truth matching instead of ghost association */
    bool m_dRmatching = false;

    /** @brief Branch name for default reco jet container.  Branches will be prepended with this name */
    std::string m_default_jet_branch_name = "jet";

    /** @brief dR for truth-to-reco matching.  Defaults to 0.3 (0.1 for R=0.2 jets).*/
    float m_truthDR = -1;
    /** @brief dR for truth jet isolation.  Defaults to 2.5 x jet radius*/
    float m_truthIsoDR = -1;
    /** @brief dR for reco jet isolation.  Defaults to 1.5 x jet radius*/
    float m_recoIsoDR = -1;

    /** @brief Boolean to save all truth jets.  Generally not needed.*/
    bool m_saveTruthJets = false;

    /** @brief Set jet scale */
    std::string m_jetScale = "";

    TreeTemplate* m_tree; //!


    /** @brief If input is MC, as automatically determined from xAOD::EventInfo::IS_SIMULATION*/
    bool m_isMC;

////// config for Jet Tools //////
    /** @brief Jet definition (e.g. AntiKt4EMTopo). Propagated option for Jet Tools */
    std::string m_jetDef;

  private:

  /**  @brief  key to retrieve the EventDensity. Constructed according to m_jetDef */
    std::string m_rhoName ="";

    /** @brief Location of primary vertex within xAOD::VertexContainer*/
    int m_pvLocation; //!

    /** @brief Event weight from the MC generation*/
    float m_mcEventWeight; //!
    /** @brief AMI cross-section weight, grabbed from file TODO*/
    float m_weight_xs; //!
    /** @brief AMI acceptance weight, grabbed from file TODO*/
    float m_weight_kfactor; //!
    /** @brief Channel number assigned to the MC sample*/
    int m_mcChannelNumber; //!
    /** @brief Run number assigned to the data*/
    int m_runNumber; //!
    /** @brief A stringstream object for various uses*/
    std::stringstream m_ss; //!

  public:
    /** @brief Vector of cutflows, for each systematic variation, showing the integer number of events passing each selection */
    std::vector<TH1D*> m_cutflowHist;    //!
    /** @brief Vector of weighted cutflows, for each systematic variation, showing the weighted number of events passing each selection */
    std::vector<TH1D*> m_cutflowHistW;   //!
    /** @brief  Bin index corresponding to the first new selection
     * @note  The cutflow histograms are grabbed from previous xAH algorithms and already include entries.
     * The first location of the new MJB entr is stored here.
     * */
    unsigned int m_cutflowFirst;     //!
    /** @brief Automatically updating index of the current selection, used by passCutAll() and passCut() */
    unsigned int m_iCutflow;     //!

  private:
    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool_handle{"InDetTrackSelectionTool"}; //!

    #ifndef __MAKECINT__

    /** @brief Vector of TreeTemplate objects to output, each corresponding to a different systematic*/
    std::vector<TreeTemplate*> m_treeList; //!
    /** @brief Retrieve event info from the xAOD::EventInfo object and the file TODO
     * @note: Fills in values for IsolatedJetAlgo#m_runNumber, IsolatedJetAlgo#m_mcChannelNumber, IsolatedJetAlgo#m_weight_xs, IsolatedJetAlgo#m_weight_kfactor */
    EL::StatusCode getSampleWeights(const xAOD::EventInfo* eventInfo);

    /** @brief Function to calculate Track Moments for various track pt selections */
    EL::StatusCode addTrackMoments(const xAOD::Jet* jet, const xAOD::Vertex* recoVertex);

    #endif

public:
    const xAOD::JetContainer* m_truthJets; //!
    const xAOD::EventInfo* m_eventInfo; //!

    float m_prescale; //!


  private:


  public:
    /** @brief Standard constructor*/
    IsolatedJetAlgo ();
//    /** @brief Standard destructor*/
//    ~IsolatedJetAlgo ();

    /** @brief Setup the job (inherits from Algorithm)*/
    virtual EL::StatusCode setupJob (EL::Job& job);
    /** @brief Execute the file (inherits from Algorithm)*/
    virtual EL::StatusCode fileExecute ();
    /** @brief Initialize the output histograms before any input file is attached (inherits from Algorithm)*/
    virtual EL::StatusCode histInitialize ();
    /** @brief Change to the next input file (inherits from Algorithm)*/
    virtual EL::StatusCode changeInput (bool firstFile);
    /** @brief Initialize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode initialize ();
    /** @brief Execute each event of the input file (inherits from Algorithm)*/
    virtual EL::StatusCode execute ();
    /** @brief End the event execution (inherits from Algorithm)*/
    virtual EL::StatusCode postExecute ();
    /** @brief Finalize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode finalize ();
    /** @brief Finalize the histograms after all files (inherits from Algorithm)*/
    virtual EL::StatusCode histFinalize ();

  // these are the functions not inherited from Algorithm
    /** @brief Configure the variables once before running*/
    virtual EL::StatusCode configure ();

    /** @brief Used to distribute the algorithm to the workers*/
    ClassDef(IsolatedJetAlgo, 1);
};

#endif
