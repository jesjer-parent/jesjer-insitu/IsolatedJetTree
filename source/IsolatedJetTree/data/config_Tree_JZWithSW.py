import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

#jetCont = "EMTopo"			
#jetCont = "EMTopoLowPt"    
#jetCont = "EMTopoNoPtCut"  
#jetCont = "LCTopo"
#jetCont = "LCTopoLowPt"
jetCont = "EMPFlowNoPtCut"   
#jetCont = "EMPFlow"
#jetCont = "EMPFlowLowPt"   # seems not to work (mc16e)

#calibSeq = "JetArea_Residual_EtaJES"
calibSeq = "" 

calibConfig = "JES_data2017_2016_2015_Consolidated_PFlow_2018_Rel21.config"
#calibConfig = "JES_data2017_2016_2015_Recommendation_PFlow_Feb2018_rel21.config"

#--Set trigger if running on data
#trigger = "HLT_j380"
trigger = ""

#Use MC16a for 2015+2016 data, and MC16d for 2017 data
mc_type = "mc16e"

if mc_type == "mc16a":

  GRLs    = [ "IsolatedJetTree/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
              "IsolatedJetTree/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
            ]
  iLumis  = [ "IsolatedJetTree/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
              "IsolatedJetTree/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-008.root",
            ]
  PRWs    = [ "IsolatedJetTree/PRW_SUSY_mc16a.root"
            ]
elif mc_type == "mc16c":
  GRLs    = [ "IsolatedJetTree/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
            ]
  iLumis =  [ "IsolatedJetTree/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
            ]
  PRWs =    [ "IsolatedJetTree/PRW_SUSY_mc16c.root"
            ]
elif mc_type == "mc16d":
  GRLs    = [ "IsolatedJetTree/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
            ]
  iLumis =  [ "IsolatedJetTree/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
            ]
  PRWs =    [ "IsolatedJetTree/PRW_SUSY_mc16d.root"
            ]
elif mc_type == "mc16e": 
  GRLs    = [ "IsolatedJetTree/data18_13TeV.physics_25ns_Triggerno17e33prim.xml"
            ]					
  iLumis =  [ "IsolatedJetTree/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
            ]
  PRWs =    [ "IsolatedJetTree/PRW_SUSY_mc16e.root"
            ]

print("#####################################")
print()
print(GRLs,iLumis, PRWs)

GRLlist = ','.join(GRLs)
Lumilist = ','.join(iLumis)
PRWlist = ','.join(PRWs)

print(GRLlist, Lumilist, PRWlist)

c.algorithm("BasicEventSelection", {
  "m_name"                    : "BasicSelection",
  "m_msgLevel"                : "info",
  "m_applyGRLCut"             : True,
  "m_derivationName"          : "JETM1Kernel",
  "m_useMetaData"             : True,
  "m_storePassHLT"            : True,
  "m_storeTrigDecisions"      : True,
  "m_applyTriggerCut"         : False,  #True for data
  "m_triggerSelection"        : trigger,
  "m_checkDuplicatesData"     : False,
  "m_applyEventCleaningCut"   : True,
  "m_applyPrimaryVertexCut"   : True,
  "m_doPUreweighting"         : True,
  "m_GRLxml"                  : GRLlist,
  "m_PRWFileNames"            : PRWlist,
  "m_lumiCalcFileNames"       : Lumilist,
  } )

c.algorithm("JetCalibrator", {
  "m_name"                   : "JetCalibrator",
  "m_msgLevel"               : "info",
  "m_inContainerName"        : "AntiKt4"+jetCont+"Jets",
  "m_jetAlgo"                : "AntiKt4"+jetCont.replace('LowPt','').replace('NoPtCut',''),
  "m_outContainerName"       : "Jets_Calib",
  "m_sort"                   : True,
  "m_calibSequence"          : calibSeq,
  "m_calibConfigFullSim"     : calibConfig, #Just use this for AFII also
  "m_calibConfigData"        : calibConfig,
  "m_forceInsitu"            : False,   #Don't force insitu calibration for data
  "m_jetCleanCutLevel"       : "LooseBad",
  "m_jetCleanUgly"           : False,
  "m_redoJVT"                : True,
  "m_systName"               : "Nominal",
  "m_systVal"                : 0,
  "m_jetCalibToolsDEV"       : False,   #Developer-level JetCalibTools config file
  } )

c.algorithm("JetSelector", {
  "m_name"                    :  "JetSelector",
  "m_msgLevel"                : "info",
  "m_pT_min"                  :  0, #7 GeV jets @ calibrated pt, default 7e3
  "m_eta_max"                 :  4.5, #Eta requirement
  "m_inContainerName"         :  "Jets_Calib",
  "m_outContainerName"        :  "Jets_Selected",
  "m_createSelectedContainer" :  True,
  "m_decorateSelectedObjects" :  True,
  "m_jetScaleType"            :  "JetConstitScaleMomentum",
  "m_doJVT"                   :  False,
  "m_cleanJets"               :  False,
  "m_cleanEvent"              :  False,
  } )


##---------------------------------------------------------------------------------------------
### GSC Configuration  ###
c.algorithm("IsolatedJetAlgo",   { "m_name"  : "IsolatedJet",
  "m_msgLevel"               : "info",

  #### Jet collection and associated observables ####
  "m_inContainerName_jets"  : "Jets_Selected",
  "m_inContainerName_truth" : "AntiKt4TruthJets",
  "m_jetDef"                : "AntiKt4"+jetCont.replace('LowPt','').replace('NoPtCut',''),

  ### TTree branch options ###
  # eventDetailStr options in xAH's EventInfo.cxx (pileup, shapeEM, caloClus, etc.)
  "m_eventDetailStr"  : "pileup shapeEM shapePF shapeEMPFLOW shapeEMPFLOWPUSB weight",
  #"m_eventDetailStr"  : "pileup shapeEM shapePF shapeEMPlowPUSB weight",
  #'m_eventDetailStr' : "pileup shapeME shapePF",
  
  # jetDetailStr options in TreeTemplate.cxx and  xAH's JetContainer.cxx (GSCVars, MoreTrackMoments, JetCalibStages, isolation, etc.)
  "m_jetDetailStr"    : "kinematic area JetConstitScale JetPileupScale",
  #"m_jetDetailStr"    : "kinematic", 
  # trigDetailStr options in xAH's HelpTreeBase.cxx (basic, passTriggers, etc.). Relevant for data
  "m_trigDetailStr"   : "",
  #"m_trigDetailStr"   : "basic passTriggers",

  ### Extra Options ###
  "m_dRmatching"      : True,   #dR matching (false will use ghost association matching)
  "m_requireIso"      : True,   #apply isolation on output (vs save decision in tree)
  "m_addTrackMoments" : False,  #Add expert-level track moments
#  "m_saveTruthJets"  : False

  } )

