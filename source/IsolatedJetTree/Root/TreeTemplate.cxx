/******************************************
 *
 * TTree production for isolated jets.
 *
 * J. Dandoy (jeff.dandoy@cern.ch)
 *
 ******************************************/
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "IsolatedJetTree/TreeTemplate.h"


TreeTemplate :: TreeTemplate(xAOD::TEvent * event, TTree* tree, TFile* file) :
  HelpTreeBase(event, tree, file, 1e3)
{
  Info("TreeTemplate", "Creating output TTree  %s", tree->GetName());
}

TreeTemplate :: ~TreeTemplate()
{
}

// event variables to add
void TreeTemplate::AddEventUser(const std::string detailStr)
{

  Info("AddEventUser","Using detailStr %s", detailStr.c_str());

  m_tree->Branch("weight", &m_weight, "weight/F");
  m_tree->Branch("rho", &m_rho, "rho/F");

  // Additional weight info (allows these to be factored out of "weight"
  if( m_isMC && detailStr.find("weight") != std::string::npos ){
    m_add_weight= true;
    m_tree->Branch("weight_xs", &m_weight_xs, "weight_xs/F");
    m_tree->Branch("weight_mcEventWeight", &m_weight_mcEventWeight, "weight_mcEventWeight/F");
    m_tree->Branch("weight_pileup", &m_weight_pileup, "weight_pileup/F");
  }

  if( detailStr.find("nJet20") != std::string::npos ){
    m_add_njet20 = true;
    m_tree->Branch("nJet20", &m_njet20);
  }

}

void TreeTemplate::AddJetsUser(const std::string detailStr, const std::string jetName)
{

  std::cout<<"Sam : AddJetsUser() ***************************"<<std::endl;

  Info("AddJetsUser","Using detailStr %s", detailStr.c_str());
  //Only save additional info for default reco jets
  if( jetName.compare("jet") != 0 )
    return;

  m_tree->Branch("jet_DetEta", &m_jet_DetEta);
  m_tree->Branch("jet_Jvt", &m_jet_Jvt);

  if( m_isMC ){
    std::cout<<"Adding MC Jet Info"<<std::endl;
    m_tree->Branch("jet_true_pt", &m_jet_true_pt);
    m_tree->Branch("jet_true_eta", &m_jet_true_eta);
    m_tree->Branch("jet_true_phi", &m_jet_true_phi);
    m_tree->Branch("jet_true_e", &m_jet_true_e);

    m_tree->Branch("jet_respE", &m_jet_respE);
    m_tree->Branch("jet_respPt", &m_jet_respPt);

    m_tree->Branch("jet_PartonTruthLabelID", &m_jet_PartonTruthLabelID);
  }

  if( detailStr.find("GSCVars") != std::string::npos ){
    m_add_GSCVars = true;
    m_tree->Branch("jet_Wtrk1000", &m_jet_Wtrk1000);
    m_tree->Branch("jet_Ntrk1000", &m_jet_Ntrk1000);
    m_tree->Branch("jet_EnergyPerSampling", &m_jet_EnergyPerSampling);
    m_tree->Branch("jet_ConstitE", &m_jet_ConstitE);
    m_tree->Branch("jet_ChargedFraction", &m_jet_ChargedFraction);
    m_tree->Branch("jet_nMuSeg", &m_jet_nMuSeg);
    m_tree->Branch("jet_n90Constituents", &m_jet_n90Constituents);

  }

  if( detailStr.find("mass") != std::string::npos ){
    m_add_jetmass = true;
    m_tree->Branch("jet_m", &m_jet_m);
    if(m_isMC) m_tree->Branch("jet_true_m", &m_jet_true_m);
  }

  if( detailStr.find("MoreTrackMoments") != std::string::npos ){
    m_add_MoreTrackMoments = true;
    m_tree->Branch("jet_Ntrk500", &m_jet_Ntrk500);
    m_tree->Branch("jet_Wtrk500", &m_jet_Wtrk500);

    m_tree->Branch("jet_Ntrk2000", &m_jet_Ntrk2000);
    m_tree->Branch("jet_Ntrk3000", &m_jet_Ntrk3000);
    m_tree->Branch("jet_Ntrk4000", &m_jet_Ntrk4000);
    m_tree->Branch("jet_Wtrk2000", &m_jet_Wtrk2000);
    m_tree->Branch("jet_Wtrk3000", &m_jet_Wtrk3000);
    m_tree->Branch("jet_Wtrk4000", &m_jet_Wtrk4000);
  }

  if( detailStr.find("JetCalibStages") != std::string::npos ){
    m_add_JetCalibStages = true;
    m_tree->Branch("jet_ConstitE", &m_jet_ConstitE);
    m_tree->Branch("jet_ConstitPt", &m_jet_ConstitPt);
    m_tree->Branch("jet_ConstitEta", &m_jet_ConstitEta);
    m_tree->Branch("jet_ConstitPhi", &m_jet_ConstitPhi);
    if(m_add_jetmass) m_tree->Branch("jet_ConstitMass", &m_jet_ConstitMass);
    m_tree->Branch("jet_PileupE", &m_jet_PileupE);
    m_tree->Branch("jet_PileupPt", &m_jet_PileupPt);
    m_tree->Branch("jet_PileupEta", &m_jet_PileupEta);
    m_tree->Branch("jet_PileupPhi", &m_jet_PileupPhi);
    if(m_add_jetmass) m_tree->Branch("jet_PileupMass", &m_jet_PileupMass);
    m_tree->Branch("jet_EME", &m_jet_EME);
    m_tree->Branch("jet_EMPt", &m_jet_EMPt);
    m_tree->Branch("jet_EMEta", &m_jet_EMEta);
    m_tree->Branch("jet_EMPhi", &m_jet_EMPhi);
    if(m_add_jetmass) m_tree->Branch("jet_EMMass", &m_jet_EMMass);
    m_tree->Branch("jet_JESE", &m_jet_JESE);
    m_tree->Branch("jet_JESPt", &m_jet_JESPt);
    m_tree->Branch("jet_JESEta", &m_jet_JESEta);
    m_tree->Branch("jet_JESPhi", &m_jet_JESPhi);
    if(m_add_jetmass) m_tree->Branch("jet_JESMass", &m_jet_JESMass);
  }

  if( detailStr.find("JetConstitScale") != std::string::npos ){
    if(!m_add_JetCalibStages){
      m_add_JetConstitScale = true;
      m_tree->Branch("jet_ConstitE", &m_jet_ConstitE);
      m_tree->Branch("jet_ConstitPt", &m_jet_ConstitPt);
      m_tree->Branch("jet_ConstitEta", &m_jet_ConstitEta);
      m_tree->Branch("jet_ConstitPhi", &m_jet_ConstitPhi);
      if(m_add_jetmass) m_tree->Branch("jet_ConstitMass", &m_jet_ConstitMass);
    }
  }

  if( detailStr.find("JetPileupScale") != std::string::npos ){
    if(!m_add_JetCalibStages){
      m_add_JetPileupScale = true;
      m_tree->Branch("jet_PileupE", &m_jet_PileupE);
      m_tree->Branch("jet_PileupPt", &m_jet_PileupPt);
      m_tree->Branch("jet_PileupEta", &m_jet_PileupEta);
      m_tree->Branch("jet_PileupPhi", &m_jet_PileupPhi);
      if(m_add_jetmass) m_tree->Branch("jet_PileupMass", &m_jet_PileupMass);
    }
  }

  if( detailStr.find("JetJESScale") != std::string::npos ){
    if(!m_add_JetCalibStages){
      m_add_JetJESScale = true;
      m_tree->Branch("jet_JESE", &m_jet_JESE);
      m_tree->Branch("jet_JESPt", &m_jet_JESPt);
      m_tree->Branch("jet_JESEta", &m_jet_JESEta);
      m_tree->Branch("jet_JESPhi", &m_jet_JESPhi);
      if(m_add_jetmass) m_tree->Branch("jet_JESMass", &m_jet_JESMass);
    }
  }

  if( detailStr.find("isolation") != std::string::npos ){
    m_add_isolation = true;
    m_tree->Branch("jet_iso_dR", &m_jet_iso_dR);
    m_tree->Branch("jet_ghostFrac", &m_jet_ghostFrac);
  }



}

void TreeTemplate::FillEventUser( const xAOD::EventInfo* eventInfo ) {

  m_weight = eventInfo->auxdecor< float >( "weight" );

  if(eventInfo->isAvailable< float >("rho") ){
    m_rho = eventInfo->auxdecor< float >("rho")/1e3;
  }else{
    m_rho = -999;
  }

  if( m_add_weight ){
    m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );
    m_weight_mcEventWeight = eventInfo->auxdecor< float >( "weight_mcEventWeight" );
    m_weight_pileup = (eventInfo->isAvailable< float >("PileupWeight") ? eventInfo->auxdecor< float >("PileupWeight") : -999);
  }

  if( m_add_njet20 ){
    if(eventInfo->isAvailable< int >("nJet20") ){
      m_njet20 = eventInfo->auxdecor< int >("nJet20");
    }else{
      m_njet20 = -999;
    }
  }

}

void TreeTemplate::FillJetsUser( const xAOD::Jet* jet, const std::string ) {

  jet->isAvailable<float>("DetectorEta") ?  m_jet_DetEta.push_back( jet->auxdata<float>("DetectorEta") ) : m_jet_DetEta.push_back(-999);
  jet->isAvailable<float>("Jvt") ?  m_jet_Jvt.push_back( jet->auxdata<float>("Jvt") ) : m_jet_Jvt.push_back(-999);

  if( m_add_jetmass ) m_jet_m.push_back(  jet->m() / 1e3 );

  if( m_isMC ){
    jet->isAvailable<float>("true_pt")  ?  m_jet_true_pt.push_back( jet->auxdata<float>("true_pt")/1e3 ) : m_jet_true_pt.push_back(-999);
    jet->isAvailable<float>("true_e")   ?  m_jet_true_e.push_back( jet->auxdata<float>("true_e")/1e3 )   : m_jet_true_e.push_back(-999);
    jet->isAvailable<float>("true_eta") ?  m_jet_true_eta.push_back( jet->auxdata<float>("true_eta") )   : m_jet_true_eta.push_back(-999);
    jet->isAvailable<float>("true_phi") ?  m_jet_true_phi.push_back( jet->auxdata<float>("true_phi") )   : m_jet_true_phi.push_back(-999);

    if( m_add_jetmass ) jet->isAvailable<float>("true_m") ?  m_jet_true_m.push_back( jet->auxdata<float>("true_m") )   : m_jet_true_m.push_back(-999);
    jet->isAvailable<float>("respPt") ?  m_jet_respPt.push_back( jet->auxdata<float>("respPt") )   : m_jet_respPt.push_back(-999);
    jet->isAvailable<float>("respE")  ?  m_jet_respE.push_back( jet->auxdata<float>("respE") )     : m_jet_respE.push_back(-999);

    jet->isAvailable<int>("PartonTruthLabelID") ?  m_jet_PartonTruthLabelID.push_back( jet->auxdata<int>("PartonTruthLabelID") ) : m_jet_PartonTruthLabelID.push_back(-999);
  }

  if( m_add_GSCVars ){

    if( ! m_add_JetCalibStages && !m_add_JetConstitScale ) // Will be added in JetCalibStages
      m_jet_ConstitE.push_back( jet->jetP4(xAOD::JetConstitScaleMomentum).e() / 1e3 );

    jet->isAvailable<std::vector<int> >("NumTrkPt1000") ? m_jet_Ntrk1000.push_back( jet->auxdata<std::vector<int> >("NumTrkPt1000").at(0) ) : m_jet_Ntrk1000.push_back( -999 );
    jet->isAvailable<std::vector<float> >("TrackWidthPt1000") ? m_jet_Wtrk1000.push_back( jet->auxdata<std::vector<float> >("TrackWidthPt1000").at(0) ) : m_jet_Wtrk1000.push_back( -999 );
    jet->isAvailable<int>("GhostMuonSegmentCount") ? m_jet_nMuSeg.push_back( jet->auxdata<int>("GhostMuonSegmentCount") ) : m_jet_nMuSeg.push_back( -999 );
    jet->isAvailable<float>("N90Constituents") ? m_jet_n90Constituents.push_back( jet->auxdata<float>("N90Constituents") ) : m_jet_n90Constituents.push_back( -999 );

    //Renormalize each layer energy into GeV
    std::vector<float> tempVector;
    if( jet->isAvailable< std::vector<float> >("EnergyPerSampling") ){
      tempVector = jet->auxdata< std::vector<float> >("EnergyPerSampling");
      for( unsigned int iJ=0; iJ < tempVector.size(); ++iJ ){
        tempVector.at(iJ) /= 1e3;  // MeV to GeV
      }
    }
    m_jet_EnergyPerSampling.push_back( tempVector );

    //Divide sum trakc Pt by constituent scale Pt
    if( jet->isAvailable<std::vector<float> >("SumPtChargedPFOPt500") ){
      float thisChargedFraction =  jet->auxdata<std::vector<float> >("SumPtChargedPFOPt500").at(0);
      thisChargedFraction /= jet->jetP4(xAOD::JetConstitScaleMomentum).Pt();
      m_jet_ChargedFraction.push_back( thisChargedFraction );
    } else {
      m_jet_ChargedFraction.push_back( -999 );
    }

  }//m_add_GSCVars

  if( m_add_MoreTrackMoments ){
    jet->isAvailable<std::vector<int> >("NumTrkPt500") ? m_jet_Ntrk500.push_back( jet->auxdata<std::vector<int> >("NumTrkPt500").at(0) ) : m_jet_Ntrk500.push_back( -999 );
    jet->isAvailable<std::vector<float> >("TrackWidthPt500") ? m_jet_Wtrk500.push_back( jet->auxdata<std::vector<float> >("TrackWidthPt500").at(0) ) : m_jet_Wtrk500.push_back( -999 );
    jet->isAvailable<std::vector<int> >("NumTrkPt2000") ? m_jet_Ntrk2000.push_back( jet->auxdata<std::vector<int> >("NumTrkPt2000").at(0) ) : m_jet_Ntrk2000.push_back( -999 );
    jet->isAvailable<std::vector<float> >("TrackWidthPt2000") ? m_jet_Wtrk2000.push_back( jet->auxdata<std::vector<float> >("TrackWidthPt2000").at(0) ) : m_jet_Wtrk2000.push_back( -999 );
    jet->isAvailable<std::vector<int> >("NumTrkPt3000") ? m_jet_Ntrk3000.push_back( jet->auxdata<std::vector<int> >("NumTrkPt3000").at(0) ) : m_jet_Ntrk3000.push_back( -999 );
    jet->isAvailable<std::vector<float> >("TrackWidthPt3000") ? m_jet_Wtrk3000.push_back( jet->auxdata<std::vector<float> >("TrackWidthPt3000").at(0) ) : m_jet_Wtrk3000.push_back( -999 );
    jet->isAvailable<std::vector<int> >("NumTrkPt4000") ? m_jet_Ntrk4000.push_back( jet->auxdata<std::vector<int> >("NumTrkPt4000").at(0) ) : m_jet_Ntrk4000.push_back( -999 );
    jet->isAvailable<std::vector<float> >("TrackWidthPt4000") ? m_jet_Wtrk4000.push_back( jet->auxdata<std::vector<float> >("TrackWidthPt4000").at(0) ) : m_jet_Wtrk4000.push_back( -999 );

  }//m_add_MoreTrackMoments

  if( m_add_JetCalibStages ){
    m_jet_ConstitPt.push_back(  jet->jetP4(xAOD::JetConstitScaleMomentum).Pt() / 1e3 );
    m_jet_ConstitEta.push_back( jet->jetP4(xAOD::JetConstitScaleMomentum).eta() );
    m_jet_ConstitPhi.push_back( jet->jetP4(xAOD::JetConstitScaleMomentum).phi() );
    m_jet_ConstitE.push_back(   jet->jetP4(xAOD::JetConstitScaleMomentum).e() / 1e3 );
    if(m_add_jetmass) m_jet_ConstitMass.push_back(   jet->jetP4(xAOD::JetConstitScaleMomentum).mass() / 1e3 );
    m_jet_EMPt.push_back(  jet->jetP4(xAOD::JetEMScaleMomentum).Pt() / 1e3 );
    m_jet_EMEta.push_back( jet->jetP4(xAOD::JetEMScaleMomentum).eta() );
    m_jet_EMPhi.push_back( jet->jetP4(xAOD::JetEMScaleMomentum).phi() );
    m_jet_EME.push_back(   jet->jetP4(xAOD::JetEMScaleMomentum).e() / 1e3 );
    if(m_add_jetmass) m_jet_EMMass.push_back(   jet->jetP4(xAOD::JetEMScaleMomentum).mass() / 1e3 );
    m_jet_PileupPt.push_back(  jet->jetP4("JetPileupScaleMomentum").pt() / 1e3 );
    m_jet_PileupEta.push_back( jet->jetP4("JetPileupScaleMomentum").eta() );
    m_jet_PileupPhi.push_back( jet->jetP4("JetPileupScaleMomentum").phi() );
    m_jet_PileupE.push_back(   jet->jetP4("JetPileupScaleMomentum").e() / 1e3 );
    if(m_add_jetmass) m_jet_PileupMass.push_back(   jet->jetP4("JetPileupScaleMomentum").mass() / 1e3 );
    m_jet_JESPt.push_back(  jet->jetP4("JetEtaJESScaleMomentum").pt() / 1e3 );
    m_jet_JESEta.push_back( jet->jetP4("JetEtaJESScaleMomentum").eta() );
    m_jet_JESPhi.push_back( jet->jetP4("JetEtaJESScaleMomentum").phi() );
    m_jet_JESE.push_back(   jet->jetP4("JetEtaJESScaleMomentum").e() / 1e3 );
    if(m_add_jetmass) m_jet_JESMass.push_back(   jet->jetP4("JetEtaJESScaleMomentum").mass() / 1e3 );
  } else {
    if( m_add_JetConstitScale ){
      m_jet_ConstitPt.push_back(  jet->jetP4(xAOD::JetConstitScaleMomentum).Pt() / 1e3 );
      m_jet_ConstitEta.push_back( jet->jetP4(xAOD::JetConstitScaleMomentum).eta() );
      m_jet_ConstitPhi.push_back( jet->jetP4(xAOD::JetConstitScaleMomentum).phi() );
      m_jet_ConstitE.push_back(   jet->jetP4(xAOD::JetConstitScaleMomentum).e() / 1e3 );
      if(m_add_jetmass) m_jet_ConstitMass.push_back(   jet->jetP4(xAOD::JetConstitScaleMomentum).mass() / 1e3 );
    }
    if( m_add_JetPileupScale ){
      m_jet_PileupPt.push_back(  jet->jetP4("JetPileupScaleMomentum").pt() / 1e3 );
      m_jet_PileupEta.push_back( jet->jetP4("JetPileupScaleMomentum").eta() );
      m_jet_PileupPhi.push_back( jet->jetP4("JetPileupScaleMomentum").phi() );
      m_jet_PileupE.push_back(   jet->jetP4("JetPileupScaleMomentum").e() / 1e3 );
      if(m_add_jetmass) m_jet_PileupMass.push_back(   jet->jetP4("JetPileupScaleMomentum").mass() / 1e3 );
    }
    if( m_add_JetJESScale ){
      m_jet_JESPt.push_back(  jet->jetP4("JetEtaJESScaleMomentum").pt() / 1e3 );
      m_jet_JESEta.push_back( jet->jetP4("JetEtaJESScaleMomentum").eta() );
      m_jet_JESPhi.push_back( jet->jetP4("JetEtaJESScaleMomentum").phi() );
      m_jet_JESE.push_back(   jet->jetP4("JetEtaJESScaleMomentum").e() / 1e3 );
      if(m_add_jetmass) m_jet_JESMass.push_back(   jet->jetP4("JetEtaJESScaleMomentum").mass() / 1e3 );
    }
  }

  if(m_add_isolation){

    jet->isAvailable<float>("GhostTruthAssociationFraction") ? m_jet_ghostFrac.push_back( jet->auxdata<float>("GhostTruthAssociationFraction") ) : m_jet_ghostFrac.push_back( -999 );
    jet->isAvailable<int>("iso_dR")   ? m_jet_iso_dR.push_back( jet->auxdata<int>("iso_dR") )     : m_jet_iso_dR.push_back( -999 );

  }//if m_add_GSCVars

}


void TreeTemplate::ClearEventUser() {
}

void TreeTemplate::ClearJetsUser(const std::string jetName ) {

  (void)jetName; //Remove warning for unused variable

  m_jet_DetEta.clear();
  m_jet_Jvt.clear();

  if( m_isMC ){
    m_jet_true_pt.clear();
    m_jet_true_eta.clear();
    m_jet_true_phi.clear();
    m_jet_true_e.clear();
    m_jet_respE.clear();
    m_jet_respPt.clear();
    m_jet_PartonTruthLabelID.clear();
  }

  if( m_add_GSCVars ){
    m_jet_ConstitE.clear();
    m_jet_Ntrk1000.clear();
    m_jet_Wtrk1000.clear();
    m_jet_EnergyPerSampling.clear();
    m_jet_ChargedFraction.clear();
    m_jet_nMuSeg.clear();
    m_jet_n90Constituents.clear();

  }

  if( m_add_JetCalibStages ){
    m_jet_ConstitPt.clear();
    m_jet_ConstitEta.clear();
    m_jet_ConstitPhi.clear();
    m_jet_ConstitE.clear();
    if(m_add_jetmass) m_jet_ConstitMass.clear();
    m_jet_EMPt.clear();
    m_jet_EMEta.clear();
    m_jet_EMPhi.clear();
    m_jet_EME.clear();
    if(m_add_jetmass) m_jet_EMMass.clear();
    m_jet_PileupPt.clear();
    m_jet_PileupEta.clear();
    m_jet_PileupPhi.clear();
    m_jet_PileupE.clear();
    if(m_add_jetmass) m_jet_PileupMass.clear();
    m_jet_JESPt.clear();
    m_jet_JESEta.clear();
    m_jet_JESPhi.clear();
    m_jet_JESE.clear();
    if(m_add_jetmass) m_jet_JESMass.clear();
  } else {
    if( m_add_JetConstitScale ){
      m_jet_ConstitPt.clear();
      m_jet_ConstitEta.clear();
      m_jet_ConstitPhi.clear();
      m_jet_ConstitE.clear();
      if(m_add_jetmass) m_jet_ConstitMass.clear();
    }
    if( m_add_JetPileupScale ){
      m_jet_PileupPt.clear();
      m_jet_PileupEta.clear();
      m_jet_PileupPhi.clear();
      m_jet_PileupE.clear();
      if(m_add_jetmass) m_jet_PileupMass.clear();
    }
    if( m_add_JetJESScale ){
      m_jet_JESPt.clear();
      m_jet_JESEta.clear();
      m_jet_JESPhi.clear();
      m_jet_JESE.clear();
      if(m_add_jetmass) m_jet_JESMass.clear();
    }
  }

  if( m_add_MoreTrackMoments ){
    m_jet_Ntrk500.clear();
    m_jet_Wtrk500.clear();
    m_jet_Ntrk2000.clear();
    m_jet_Ntrk3000.clear();
    m_jet_Ntrk4000.clear();
    m_jet_Wtrk2000.clear();
    m_jet_Wtrk3000.clear();
    m_jet_Wtrk4000.clear();
  }

  if( m_add_jetmass ){
    m_jet_m.clear();
    if(m_isMC) m_jet_true_m.clear();
  }

  if( m_add_isolation ){
    m_jet_iso_dR.clear();
    m_jet_ghostFrac.clear();
  }
}
