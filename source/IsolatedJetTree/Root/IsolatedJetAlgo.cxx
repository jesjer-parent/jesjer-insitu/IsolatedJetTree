/******************************************
 *
 * Algorithm to perform isolated jet event selection.
 *
 * J. Dandoy (jeff.dandoy@cern.ch)
 *
 ******************************************/

// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"

// EDM include(s):
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTracking/VertexContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODEventShape/EventShape.h"

#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthEventContainer.h"

#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

#include "PathResolver/PathResolver.h"

// ROOT include(s):
#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TKey.h"

// c++ includes(s):
#include <iostream>
#include <fstream>

// package include(s):
#include <IsolatedJetTree/IsolatedJetAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "IsolatedJetTree/IsolationHelpers.h"

using namespace std;
using namespace xAH;

// this is needed to distribute the algorithm to the workers
ClassImp(IsolatedJetAlgo)



IsolatedJetAlgo :: IsolatedJetAlgo () :
  Algorithm("IsolatedJet")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

}

EL::StatusCode  IsolatedJetAlgo :: configure (){

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode IsolatedJetAlgo :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  xAOD::Init( "IsolatedJetAlgo" ).ignore(); // call before opening first file

  EL::OutputStream outForTree("tree");
  job.outputAdd (outForTree);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode IsolatedJetAlgo :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_MSG_INFO( "Calling histInitialize");
  ANA_CHECK( xAH::Algorithm::algInitialize())

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode IsolatedJetAlgo :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode IsolatedJetAlgo :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  (void)firstFile; //surpress unused param warning
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode IsolatedJetAlgo :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  m_eventCounter = -1;

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK( HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store, msg()) );
  m_isMC = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? true : false;

  ANA_CHECK(getSampleWeights(eventInfo) );

  // Add track moments if requested
  if( m_addTrackMoments ){
    setToolName( m_trkSelTool_handle );
    ANA_CHECK( ASG_MAKE_ANA_TOOL(m_trkSelTool_handle, InDet::InDetTrackSelectionTool) );
    ANA_CHECK( m_trkSelTool_handle.setProperty("OutputLevel", msg().level()) );
    ANA_CHECK( m_trkSelTool_handle.retrieve() );
  }

  ANA_MSG_INFO("Initializing TTrees");
  TFile * treeFile = wk()->getOutputFile ("tree");
  if( !treeFile ) {
    ANA_MSG_ERROR("Failed to get file for output tree!");
    return EL::StatusCode::FAILURE;
  }

  TTree * outTree = new TTree( "IsolatedJet_tree", "IsolatedJet_tree" );
  if( !outTree ) {
    ANA_MSG_ERROR("Failed to get output tree!");
    return EL::StatusCode::FAILURE;
  }
  outTree->SetDirectory( treeFile );
  m_tree = new TreeTemplate(m_event, outTree, treeFile);
  m_tree->m_vertexContainerName = m_vertexContainerName;

  m_tree->AddEvent( m_eventDetailStr );
  if( m_addTrackMoments ){ //If calculating track moments, make sure they get saved
    m_jetDetailStr += " MoreTrackMoments";
  }
  if( m_requireIso == false ){ //If no isolation is required, make sure decision is saved
    m_jetDetailStr += " isolation";
  }
  ANA_MSG_INFO("Sam Adding jet info : "<<m_jetDetailStr);
  m_tree->AddJets( m_jetDetailStr, m_default_jet_branch_name );


  if( m_saveTruthJets )
    m_tree->AddJets( "kinematic", "truthJet" );

  //Figure out jet radius (m_jetDef is like AntiKt4EMTopo) //
  std::string jetRstr = m_jetDef;
  if( jetRstr.find("AntiKt") != std::string::npos )
    jetRstr.erase(jetRstr.find("AntiKt"), 6);
  if( jetRstr.find("EMTopo") != std::string::npos )
    jetRstr.erase(jetRstr.find("EMTopo"), 6);
  if( jetRstr.find("LCTopo") != std::string::npos )
    jetRstr.erase(jetRstr.find("LCTopo"), 6);
  if( jetRstr.find("CSSKTopo") != std::string::npos )
    jetRstr.erase(jetRstr.find("CSSKTopo"), 6);
  if( jetRstr.find("EMPFlow") != std::string::npos )
    jetRstr.erase(jetRstr.find("EMPFlow"), 7);
  if( jetRstr.find("LowPt") != std::string::npos )
    jetRstr.erase(jetRstr.find("LowPt"), 5);

  m_jetRadius = std::stof(jetRstr)/10.;
  ANA_MSG_INFO("Using jet of radius " << m_jetRadius);
  ANA_MSG_INFO("Succesfully initialized output TTree!");


  // Set truth matching & isolated dR requirements if not already set //
  if( m_truthDR == -1 ){
    m_truthDR = 0.3;
    // Stricter dR matching requiremnt for small-R jets //
    if( m_jetRadius <= 0.2 )
      m_truthDR = 0.1;
  }

  if( m_truthIsoDR == -1 ){
    m_truthIsoDR = 2.5*m_jetRadius;
  }
  if( m_recoIsoDR == -1 ){
    m_recoIsoDR = 1.5*m_jetRadius;
  }


  // Retrieve Rho container name from m_jetDef (like AntiKt4EMTopo) //
  m_rhoName = m_jetDef;
  if( m_rhoName.find("LowPt") != std::string::npos )
    m_rhoName.erase(m_rhoName.find("LowPt"), 5);

  if( m_rhoName.find("Anti") != std::string::npos )
    m_rhoName.erase(m_rhoName.find("Anti"), 4);
  else{
    ANA_MSG_ERROR("Cannot get rho topology container from jet definition " << m_jetDef);
    return EL::StatusCode::FAILURE;
  }
  if(m_rhoName.find("Topo") != std::string::npos ){
    m_rhoName += "Origin";
  }
  if (m_jetDef == "AntiKt4CSSKTopo") m_rhoName = "Kt4EMTopoOrigin";

  m_rhoName += "EventShape";
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode IsolatedJetAlgo :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ++m_eventCounter;
  ANA_MSG_VERBOSE("Begin Execute " << m_eventCounter);

  if(m_eventCounter %100000 == 0)
    ANA_MSG_INFO("Event # " << m_eventCounter);

  m_iCutflow = m_cutflowFirst; //for cutflow histogram automatic filling

  //----------------------------
  // Event information
  //---------------------------
  ///////////////////////////// Retrieve Containers /////////////////////////////////////////
  ANA_MSG_DEBUG("Retrieve Containers");

  m_eventInfo = 0;
  ANA_CHECK( HelperFunctions::retrieve(m_eventInfo, "EventInfo", m_event, m_store, msg()) );

  // Get reco vertex container and the hard scatter vertex //
  const xAOD::VertexContainer* vertices = 0;
  ANA_CHECK( HelperFunctions::retrieve(vertices, m_vertexContainerName, m_event, m_store, msg()) );
  int pvLocation = m_applyVertexCut ? HelperFunctions::getPrimaryVertexLocation(vertices) : 0;
  const xAOD::Vertex* recoVertex = vertices->at(pvLocation);

  // Get a vector of valid reco jets //
  xAOD::JetContainer* recoJets = nullptr; // will be filled if anlyzing L1 jets
  std::vector< const xAOD::Jet*> recoJetVec;

  // HLT/offline jets or L1 jets?
  bool isL1 = false;
  if(m_inContainerName_jets.find("L1")!=std::string::npos || m_inContainerName_jets.find("jRound")!=std::string::npos){
    isL1 = true;
  }


  if(!isL1){
    // Get reco jets //
    const xAOD::JetContainer*    inOfflineJets = 0;    
    ANA_CHECK( HelperFunctions::retrieve(inOfflineJets, m_inContainerName_jets, m_event, m_store, msg()) );
    for( auto thisJet : *inOfflineJets ) {
      // No negative energy jets
      if(thisJet->e() < 0) continue;
      // (pt and eta requirement already happened in JetSelector)
      recoJetVec.push_back(thisJet);
    }
  } else {
    // Get L1 jets //
    recoJets = new xAOD::JetContainer; // !!! THIS HAS TO BE DELETED SOMEWHERE !!!
    recoJets->setStore(new xAOD::JetAuxContainer);

    const xAOD::JetRoIContainer* inL1Jets      = 0;
    ANA_CHECK( HelperFunctions::retrieve(inL1Jets, m_inContainerName_jets, m_event, m_store, msg()) );
    for( auto thisJet : *inL1Jets ) {
      float pt  = thisJet->et8x8();
      float eta = thisJet->eta();
      float phi = thisJet->phi();
      float m   = 0.;
      // No negative pt jets
      if(pt<0) continue;
      xAOD::Jet* jet = new xAOD::Jet();
      recoJets->push_back(jet);
      // (pt and eta requirement already happened in JetSelector)
      jet->setJetP4(xAOD::JetFourMom_t(pt,eta,phi,m));
      recoJetVec.push_back(jet);
    }
  }


  m_truthJets = 0;
  //std::vector< const xAOD::Jet*>* truthJetVec = new std::vector< const xAOD::Jet* >();
  std::vector< const xAOD::Jet*> truthJetVec;
  const xAOD::TruthVertexContainer* truth_vertices = 0;
  const xAOD::TruthVertex* truthVertex = nullptr;
  if(m_isMC){

    // Get truth jets //
    ANA_CHECK( HelperFunctions::retrieve(m_truthJets, m_inContainerName_truth, m_event, m_store, msg()) );
    // Get a vector of valid truth jets //
    for( auto thisJet : *m_truthJets ) {
      // No negative energy jets
      if (thisJet->e() < 0)
        continue;
      if( fabs(thisJet->pt()) < 7 )  //Only truth jets above 7 GeV
        continue;
      if( fabs(thisJet->eta()) > 5 ) //Only truth jets with eta of 5
        continue;
       truthJetVec.push_back( thisJet );
     }

    // Get truth vertex container and the true hard scatter vertex //
    if(m_applyVertexCut){
      ANA_CHECK( HelperFunctions::retrieve(truth_vertices, "TruthVertices", m_event, m_store, msg()) );
      int thisBarcode = -999;
      for( auto *vertex : *truth_vertices ){
          if( vertex->barcode() < 0 && vertex->barcode() > thisBarcode ){
            thisBarcode = vertex->barcode();
            truthVertex = vertex;
          }
      }
    }
  }//m_isMC


  const xAOD::EventShape* evtshape = nullptr;
  ANA_CHECK( HelperFunctions::retrieve(evtshape, m_rhoName, m_event, m_store, msg()) );
  double rho;
  evtshape->getDensity(xAOD::EventShape::Density, rho);

  ///////////////////////// Begin Isolated Jet selections //////////////////////////////////

  // At least 1 truth jet //
  if ( m_isMC && truthJetVec.size() <= 0 ){
    ANA_MSG_DEBUG("Zero truth jets");
    return EL::StatusCode::SUCCESS;
  }

  // At least 2 reco jets //
  if ( recoJetVec.size() < 2 ){
    ANA_MSG_DEBUG("Less than 2 reco jets");
    return EL::StatusCode::SUCCESS;
  }

  // Check against pile-up only jets //
  if( m_isMC ){
    float pTAvg = 0.;
    if(m_jetScale==""){
      pTAvg = ( recoJetVec[0]->pt() + recoJetVec[1]->pt() ) / 2.0;
    } else  { // use chosen JetScaleMomentum
      xAOD::JetFourMom_t tmp;
      if(!recoJetVec[0]->getAttribute<xAOD::JetFourMom_t>(m_jetScale.c_str(),tmp)){
        ATH_MSG_ERROR("Jet does not have the requested momentum state: " << m_jetScale);
        return EL::StatusCode::FAILURE;
      } else {
        pTAvg = ( recoJetVec[0]->jetP4(m_jetScale.c_str()).Pt() + recoJetVec[1]->jetP4(m_jetScale.c_str()).Pt() ) / 2.0;
      }
    }
    if( (pTAvg / truthJetVec[0]->pt() > 1.4) ){
      ANA_MSG_DEBUG("Failed truth pileup requirement.");
      return EL::StatusCode::SUCCESS;
    }
  }


  // Needs reco and truth verticies
  if( (m_isMC && !truthVertex) || !recoVertex ){
    if(m_applyVertexCut){
      ANA_MSG_DEBUG("No truth or reco vertex");
      return EL::StatusCode::SUCCESS;
    }
  }
  //Reco & truth verticies must be close
  if(m_isMC && m_applyVertexCut && fabs(recoVertex->z() - truthVertex->z()) > 0.2){
    ANA_MSG_DEBUG("Failing PVZ cut with " << recoVertex->z() << " VS " << truthVertex->z());
    return EL::StatusCode::SUCCESS;
  }


  // Clear the input TTree vectors before filling any new jets //
  m_tree->ClearJets();
  bool filledTree = false;
  int njet20 = 0;

  IsolatedJetTree::IsoHelper isohelper(m_jetScale, m_truthDR, m_truthIsoDR, m_recoIsoDR, !m_dRmatching, 0.75);
  
  for( auto jet : recoJetVec ){

    xAOD::JetFourMom_t jetconstitP4 = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
    if( jetconstitP4.pt()/1e3 > 20.) njet20++;

    ///////////////////////// Check isolation and truth matching  //////////////////////////////
    IsolatedJetTree::IsoHelper::Result isoResult;
    if(m_isMC)
      isoResult = isohelper.checkTruthMatchAndIso( jet, truthJetVec, recoJetVec);
    else
      isoResult = isohelper.checkTruthMatchAndIso( jet, recoJetVec);
    

    if(m_isMC && (!isoResult.isTruthMatched) ){
      ANA_MSG_DEBUG("No matched truth jet");
      continue;
    }

    if( m_requireIso  ){
      if( m_isMC && (!isoResult.isTruthIsolated) ){ continue; }
      ANA_MSG_DEBUG("Passed truth jet isolation requirement ");
      if(!isoResult.isRecoIsolated){ continue; }
      ANA_MSG_DEBUG("Passed reco jet isolation requirement");
    }

    ///////////////////////// Save Jet output  //////////////////////////////////
    ANA_MSG_DEBUG("Filling TTree for this jet " << jet->index());
    if( m_isMC ){
      const xAOD::Jet* matchedTruthJet = isoResult.matchedTruthJet;
      jet->auxdecor< float >("true_pt") = matchedTruthJet->pt();
      jet->auxdecor< float >("true_eta") = matchedTruthJet->eta();
      jet->auxdecor< float >("true_phi") = matchedTruthJet->phi();
      jet->auxdecor< float >("true_e") = matchedTruthJet->e();
      jet->auxdecor< float >("true_m") = matchedTruthJet->m();
      jet->auxdecor< float >("respPt") = jet->pt()/matchedTruthJet->pt();
      jet->auxdecor< float >("respE") = jet->e()/matchedTruthJet->e();
    }

    jet->auxdecor< int >("iso_dR") = isoResult.isRecoIsolated;

    // Add 2, 3, and 4 GeV track moments //
    if( m_addTrackMoments ){
      addTrackMoments(jet, recoVertex);
    }

    m_tree->FillJet( jet, recoVertex, pvLocation );

    filledTree = true;
  }

  //Skip the rest if no jet passed
  if (filledTree == false){
    ANA_MSG_DEBUG("No isolated reco jets in this event");
    return EL::StatusCode::SUCCESS;
  }

  //Save all truth jets
  if( m_saveTruthJets ){
    m_tree->FillJets( m_truthJets, 0, "truthJet");
  }


  ///////////////////////// Save event output  //////////////////////////////////
  ANA_MSG_DEBUG("Filling TTree for the event");
  m_mcEventWeight = (m_isMC ? m_eventInfo->mcEventWeight() : 1.) ;
  m_eventInfo->auxdecor< float >("weight_xs") = m_weight_xs * m_weight_kfactor;
  float weight_pileup = 1.;
  if(m_eventInfo->isAvailable< float >("PileupWeight") ){
    weight_pileup = m_eventInfo->auxdecor< float >("PileupWeight");
  }
  m_eventInfo->auxdecor< float >("weight") = m_mcEventWeight*m_weight_xs*m_weight_kfactor*weight_pileup;

  m_eventInfo->auxdecor< float >("rho") = rho;

  m_eventInfo->auxdecor< int >("nJet20") = njet20;

  m_tree->FillEvent( m_eventInfo, m_event, vertices );  
  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode IsolatedJetAlgo :: postExecute ()
{
  ANA_MSG_VERBOSE("postExecute()");
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode IsolatedJetAlgo :: finalize ()
{
  ANA_MSG_INFO("finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.


  // Get MetaData_EventCount histogram
  TFile* metaDataFile = wk()->getOutputFile( "metadata" );
  TH1D* metaDataHist = (TH1D*) metaDataFile->Get("MetaData_EventCount");

  // Save MetaData_EventCount to data-tree
  TFile *treeFile = wk()->getOutputFile( "tree" );
  TH1D* thisMetaDataHist = (TH1D*) metaDataHist->Clone();
  thisMetaDataHist->SetDirectory( treeFile );

  ANA_MSG_INFO("Done finalize");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode IsolatedJetAlgo :: histFinalize ()
{
  ANA_MSG_INFO("histFinalize()");
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.


  ANA_CHECK( xAH::Algorithm::algFinalize());
  return EL::StatusCode::SUCCESS;
}


//This grabs luminosity, acceptace, and eventNumber information from the respective text file
EL::StatusCode IsolatedJetAlgo::getSampleWeights(const xAOD::EventInfo* eventInfo) {
  ANA_MSG_INFO("getSampleWeights()");

  float weight_xs = 1.0, weight_kfactor = 1.0, weight_eff = 1.0;
  if(!m_isMC){
    m_runNumber = eventInfo->runNumber();
  }else{
    m_mcChannelNumber = eventInfo->mcChannelNumber();
    ANA_MSG_INFO("m_mcChannelNumber is " << m_mcChannelNumber);
    ifstream fileIn(  PathResolverFindCalibFile( "IsolatedJetTree/PMGxsecDB_mc16.txt" ) );

    // Search the input file for the correct MCChannelNumber, and find the XS and kfactor numbers
    std::string mcChanStr = std::to_string( m_mcChannelNumber );
    bool foundXS = false;
    std::string line;
    std::string subStr;
    while (getline(fileIn, line)){
      istringstream iss(line);
      // DSID should be first item in the line, so put it into subStr and see if mcChanStr matches
      iss >> subStr;
      if (subStr.find(mcChanStr) != std::string::npos){
        iss >> subStr; // Get Name
        iss >> subStr; // Get XS (ipb)
        sscanf(subStr.c_str(), "%e", &weight_xs);
        iss >> subStr; // Get kfactor
        sscanf(subStr.c_str(), "%e", &weight_kfactor);
        iss >> subStr; // Get Efficiency
        sscanf(subStr.c_str(), "%e", &weight_eff);
        //iss >> subStr; // Get relative uncertainty
        ANA_MSG_INFO("Setting xs=" << weight_xs << ", efficiency=" << weight_eff << ", acceptance=" << weight_kfactor);
        foundXS = true;
        break;
      }
    }
    if( !foundXS){
      cerr << "ERROR: Could not find proper file information for MC sample " << mcChanStr << endl;
      return EL::StatusCode::FAILURE;
    }

    // Decorate eventInfo with information //
    m_weight_xs = weight_xs*weight_eff;
    m_weight_kfactor = weight_kfactor;
  }

  return EL::StatusCode::SUCCESS;
}

// Add 2, 3, and 4 GeV track moments //
EL::StatusCode IsolatedJetAlgo :: addTrackMoments(const xAOD::Jet* jet, const xAOD::Vertex* recoVertex)
{
  static SG::AuxElement::ConstAccessor< std::vector<ElementLink<DataVector<xAOD::IParticle> > > >ghostTrack ("GhostTrack");
  std::vector<ElementLink<DataVector<xAOD::IParticle> > > trackLinks = ghostTrack( *jet );

  std::vector<int> NumTrk2, NumTrk3, NumTrk4;
  std::vector<float> WidthTrk2, WidthTrk3, WidthTrk4;
  std::vector<float> SumPtTrk2, SumPtTrk3, SumPtTrk4;
  NumTrk2.push_back(0); NumTrk3.push_back(0); NumTrk4.push_back(0);
  WidthTrk2.push_back(0); WidthTrk3.push_back(0); WidthTrk4.push_back(0);
  SumPtTrk2.push_back(0); SumPtTrk3.push_back(0); SumPtTrk4.push_back(0);
  for ( auto link_itr : trackLinks ) {
    if( !link_itr.isValid() ) { continue; }
    const xAOD::TrackParticle* track = dynamic_cast<const xAOD::TrackParticle*>( *link_itr );
    // if asking for tracks passing PV selection ( i.e. JVF JVT tracks ) PV selection from
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JvtManualRecalculation
    if( !m_trkSelTool_handle->accept(*track,recoVertex) ) { continue; } // ID quality cut
    if( track->vertex() != recoVertex ) { // if not in PV vertex fit
      if( track->vertex() != 0 ) { continue; } // make sure in no vertex fits
      if( fabs((track->z0()+track->vz()-recoVertex->z())*sin(track->theta())) > 3.0 ) { continue; } // make sure close to PV in z
    }

    //http://acode-browser.usatlas.bnl.gov/lxr/source/athena/Reconstruction/Jet/JetMomentTools/Root/JetTrackMomentsTool.cxx
    float deltaR = jet->p4().DeltaR( track->p4() );

    if( track->pt() < 2000 ){ continue; } // pT cut
    NumTrk2.at(0)++;
    SumPtTrk2.at(0) += track->pt();
    WidthTrk2.at(0) += deltaR * track->pt();

    if( track->pt() < 3000 ){ continue; } // pT cut
    NumTrk3.at(0)++;
    SumPtTrk3.at(0) += track->pt();
    WidthTrk3.at(0) += deltaR * track->pt();

    if( track->pt() < 4000 ){ continue; } // pT cut
    NumTrk4.at(0)++;
    SumPtTrk4.at(0) += track->pt();
    WidthTrk4.at(0) += deltaR * track->pt();
  }
  WidthTrk2.at(0) = SumPtTrk2.at(0) > 0 ? WidthTrk2.at(0) / SumPtTrk2.at(0) : -1;
  WidthTrk3.at(0) = SumPtTrk3.at(0) > 0 ? WidthTrk3.at(0) / SumPtTrk3.at(0) : -1;
  WidthTrk4.at(0) = SumPtTrk4.at(0) > 0 ? WidthTrk4.at(0) / SumPtTrk4.at(0) : -1;

  jet->auxdecor< std::vector<int> >("NumTrkPt2000") = NumTrk2;
  jet->auxdecor< std::vector<int> >("NumTrkPt3000") = NumTrk3;
  jet->auxdecor< std::vector<int> >("NumTrkPt4000") = NumTrk4;
  jet->auxdecor< std::vector<float> >("TrackWidthPt2000") = WidthTrk2;
  jet->auxdecor< std::vector<float> >("TrackWidthPt3000") = WidthTrk3;
  jet->auxdecor< std::vector<float> >("TrackWidthPt4000") = WidthTrk4;

  return EL::StatusCode::SUCCESS;
}
