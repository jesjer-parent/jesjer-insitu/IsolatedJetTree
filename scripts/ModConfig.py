#!/usr/bin/python

import sys,os,getopt

def main(argv):

  # parse the input arguments
  inputfile = 'xxx'
  mods      = 'xxx'
  try:
    opts, args = getopt.getopt(argv,"hi:m:",["ifile=","ofile="])
  except getopt.GetoptError:
    print('ModConfig.py -i <inputfile> -m <list of mods>')
    sys.exit(2)
  for opt, arg in opts:
    if opt in   ("-h"):
      print('USAGE : ModConfig.py -i <inputfile> -m <list of mods>')
      sys.exit(2)
    if opt in   ("-i", "--ifile"):
      inputfile  = arg
    elif opt in ("-m", "--mods"):
      mods   = arg

  # check for the necessary arguments        
  if inputfile=='xxx':
    print("Need an inputfile ... exitting")
    sys.exit(1)
    
  if mods=='xxx':
    print("Need some mods or why are you running this ... exitting")
    sys.exit(1)     
    
  # parse the mods according to the fixed ordering
  # [arg,newval]
  
  mods_dict={}
  for mod in mods.split('|'):
    stripmod = mod.strip()
    #print(stripmod)
    if stripmod[0]!='[':
      print("Improper mod formatting start, exitting")
      sys.exit(3)
    if stripmod[-1]!=']':
      print("Improper mod formatting end, exitting")
      sys.exit(3)
      
    mod_key   = stripmod[1:-1].split(',')[0]
    mod_entry = stripmod[1:-1].split(',')[1]
    
    #print(mod_key,mod_entry)
  
    mods_dict[mod_key] = mod_entry
  
  # print the relevant data for running
  print('Input file is : ')
  print('  ',inputfile)
  print('Mods are      : ')
  for key in list(mods_dict.keys()):
    print('  ',key,' (newval)--> ',mods_dict[key])
    
    
  # check that the old file exists and open it
  if not os.path.exists(inputfile):
    print("Can't find input file ... exitting")
    sys.exit(3)
  
  fin  = open(inputfile,"r")
  
  # open the new file to which we will write
  fout = open("tempModConfig.txt","w")
  
  # print and replace value if key exists
  used_mods=[]
  
  for line in fin:
    print("----")
    print(line,len(line))
    
    if len(line)<=1:    # nothing is there
      newline = line
    elif isCommentLine(line):
      newline = line  
    elif "=" not in line:
      newline = line  
    elif line.count("=")>1:
      newline = line
    else:
      keylookup = line.strip().split("=")[0].strip()
      key       = line.split("=")[0]
      entry     = line.strip().split("=")[1]
      
      if keylookup in mods_dict.keys():
        entry = mods_dict[keylookup]
        used_mods.append(keylookup)
        
      newline = key+" = "+entry+"\n"
      
    fout.write(newline)
    
  # list all the mods that were used for verification
  print("Which mods were used?")
  for mod in mods_dict.keys():
    if mod in used_mods:
      print(mod," : Used     -->(newval) ",mods_dict[mod])
    else:
      print(mod," : Not Used")
    
  # close the files
  fin.close()
  fout.close()
  
  
      
      
def isCommentLine(line):
  # checks if the only characters in the stripped line are spaces or #
  
  # there is nothing on it
  if len(line)<=1:
    return True
    
  # if it starts with a #
  if line[0]=="#":
    return True
  
  # interesting, let's check each letter
  for i in range(len(line.strip())):
    #print(line[i])
    if line[i]!="#" and line[i]!=" ":
      return False
      
  return True   
  

if __name__ == "__main__":
   main(sys.argv[1:])