#!/usr/bin/python

import sys, getopt
from ROOT import TH1F,TCanvas,TFile,gDirectory

def main(argv):

  # parse the input arguments
  inputfile = 'xxx'
  treename  = 'xxx'
  histname  = 'xxx'
  outname   = 'xxx'
  try:
    opts, args = getopt.getopt(argv,"hp:i:t:o:",["ifile=","ofile="])
  except getopt.GetoptError:
    print('DrawHist.py -i <inputfile> -t <treename> -p <histname> -o <outputpath>')
    sys.exit(2)
  for opt, arg in opts:
    if opt in   ("-h"):
      print('USAGE : DrawHist.py -i <inputfile> -p <histname> -o <outputpath>')
      sys.exit(2)
    if opt in   ("-i", "--ifile"):
      inputfile  = arg
    if opt in   ("-t", "--tree"):
      treename   = arg
    elif opt in ("-p", "--plot"):
      histname   = arg
    elif opt in ("-o", "--ofile"):
      outname    = arg

  # check for the necessary arguments        
  if inputfile=='xxx':
    print("Need an inputfile ... exitting")
    sys.exit(1)
    
  if treename=='xxx':
    print("Need an treename ... exitting")
    sys.exit(1)
    
  if histname=='xxx':
    print("Need a histname ... exitting")
    sys.exit(1)    
    
  if outname=='xxx':
    print("Need an output location ... exitting")
    sys.exit(1)    
  
  # print the relevant data for running
  print('Input file is   : ', inputfile)
  print('Treename is     : ', treename)
  print('Hist to plot is : ', histname)
  print('Output file is  : ', outname)
  
  # extract and draw the histogram  
  fin = TFile(inputfile)
  fin.ls()
  t   = fin.Get(treename)
  t.Print()
  
  t.Draw(histname+">>htemp")
  h = gDirectory.Get("htemp")
  
  
#  h = fin.Get(histname)
  
  c = TCanvas("c","c",500,500)
  h.Draw()
  c.SaveAs(outname)
        

if __name__ == "__main__":
   main(sys.argv[1:])