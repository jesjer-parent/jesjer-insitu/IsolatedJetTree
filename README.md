# IsolatedJetTree
This package creates TTrees of isolated reco jets matched to truth jets for use in deriving the MCJES, GSC, or uncertainties of the ATLAS jet energy scale calibration.
While it is mainly for running over QCD multijet JZ slices in Monte Carlo, it can also run over data.
It is built upon the [xAODAnaHelpers](https://xaodanahelpers.readthedocs.io/en/latest/) (xAH) package, benefiting from algorithms for basic event selection, jet calibration, and jet selection.
It also takes advantage of xAH's skeleton for creating TTrees, with easy access to additional output branches via the detailStr.

This package was created and is maintained by Jeff Dandoy (jeff.dandoy AT cern.ch).


## Get packages

Your top directory should include two direcdtories: `build` and `source`.
Packages will be placed in the source directory, and will be compiled from the build directory.
Commands should be run from the top directory, outside of both `build` and `source`.
```
mkdir source
mkdir build
```

Within the source directory, checkout this package with a command like:
```
git clone ssh://git@gitlab.cern.ch:7999/atlas-jetetmiss-jesjer/MCcalibrations/IsolatedJetTree.git
```
The above assumes you have a ssh key connected to your gitlab account, following the procedure described [here](https://docs.gitlab.com/ee/ssh/).
You can also find links for cloning via http or kerberos on [the current page](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/MCcalibrations/IsolatedJetTree).

This package is built on the xAH, which may be downloaded with:
```
git clone https://github.com/UCATLAS/xAODAnaHelpers.git
cd xAODAnaHelpers/
git checkout fa2ea40b938e45
```

This is the last tested commit of xAH, but the lastest master is likely compatible.

## Setup environment

The last tested analysis base is AnalysisBase 21.2.62, but newer versions are likely compatible.
Analysis Base should always be setup in source, and the code compiled with cmake in the build directory.
```
setupATLAS
cd source/
asetup AnalysisBase,21.2.62,here
cd ../build
cmake ../source
make
source x86_64-slc6-gcc62-opt/setup.sh
cd ../
```
All commands should be run from the top directory.

## Understanding and running analysis code locally

You should always run the analysis code locally and check the TTree output to make sure things are working.
This is especially true if you are making local edits to the algorithm.

The analysis code runs over JETM1 dijet MC and outputs TTrees with the requested jet variables.
The Eventloop algorithms are run via xAODAnaHelpers through the xAH_run.py script.
This is a fairly complex script with a large deal of functionality, so don't worry about understanding the details of how it works at first!
The EventLoop algorthims that are run, and their variable settings, are given in config files, such as IsolatedJetTree/data/config_Tree.py.
These define which algorithms are run and their order, with BasicEventSelection first, then JetCalibrator, then JetSelector, and finally the IsolatedJetAlgo.cxx algorithm.
Further details on the config files is given below.

To run over a DAOD file locally, you can use a command like
```
python source/xAODAnaHelpers/scripts/xAH_run.py --config source/IsolatedJetTree/data/config_Tree.py --files path/to/DAODFile.root --force direct
```
The files option should usually point to a single root file for testing purposes.
It is possible to point to a directory of several files, a text file with paths to many input files (`--inputList`), wildcard the input files to use (`--inputTag`), or scan for files using rucio (`--inputRucio`), EOS (`--inputEOS`), XRD (`--scanXRD`), or SH (`--inputSH`).

## Grid submission and download files
To run over all JZ slices on the grid, use:
```
python source/IsolatedJetTree/scripts/runGrid.py
```
The user should first edit the top of this file to point to the correct config file and input container names to search for (wildcarded).
The `extraTag` appends a string to the output container name (therefore keep it short!), allowing for clarity and uniqueness in the output file name.
Jobs can be monitored with the panda user interface.

The grid output samples can be downloaded locally with the downloadAndMerge script.  The output files are TTrees, and can be downloaded with a command such as:
```
python source/IsolatedJetTree/scripts/downloadAndMerge.py --types tree --container user.jdandoy.36\*.Pythia_EM4_300517
```
The output will be placed in the gridOutput/tree/ directory.  The unmerged files will be stored in gridOutput/rawDownload, and may be deleted after all files are succesfully downloaded.
The files for each JZ slice will be merged automatically, but the various JZ slices will remain separate.

## Config file details & additional options

The top of the config file, like `data/config_Tree.py`, includes shortcuts to set the jet container type & calibration config file, allowing them to be quickly changed.
It also includes a list of GRL, ilumicalc, and PRW files, which can be selected depending on the mc version usec (e.g. mc16a vs mc16d).

Within the IsolatedJetAlgo at the end of the config, there are several additional options:

* dR matching between truth and reco jets is the default.
GhostTruthAssociation matching can be used by setting m_dRmatching to false.

* There is functionality to study various isolation working points.
By setting m_requireIso to false in your local version of config_Tree.py, only the loosest isolation requirement will be applied, and the various per-jet isolation decisions will be saved in the TTree.
Different isolation WPs can then be tests later.
If m_requireIso is set to true, the default working point (halfPt) will be used.

* Ntrk & Wtrk can be studied with higher pt thresholds (2, 3, and 4 GeV) by setting m_addTrackMoments to true.

The branches added to the TTree are controlled with the various DetailStr.
Many options are built into xAH to allow quick access to observables of various types, such as cleaning or kinematic variables.
There are also special detailStr options for the IsolatedJet code, which are detailed below.

`m_eventDetailStr` controls the event-level observables.
One detailStr variable, `weight`, adds the individual weights to the TTree output, allowing them to be factorized out of the single `weight` branch that is provided by default.

`m_jetDetailStr` controls the jet-level observables.
The following options are available in IsolatedJetTree:
* GSCVars : Add variables necessary for the GSC
* MoreTrackMoments : Include Ntrk and Wtrk using track pt thresholds of 0.5, 2, 3, and 4 GeV
* JetCalibStages : Include EM, Constituent, and Pileup scale jet kinematics
* isolation : Include isolation decisions for various working points

# Previous Versions of note
*Tag Recommendations_2018*: AnalysisBase,21.2.28 w/ xAODAnaHelpers 32da68f7bf11
